<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;
use App\Services\ProductGenerator;

class ProductRepository implements ProductRepositoryInterface
{
    private $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products = [])
    {
        $this->products = count($products) > 0 ? $products : ProductGenerator::generate();
    }

    /**
     * @return Product[]
     */
    public function findAll(): array
    {
        return $this->products;
    }

    public function findMostPopularProduct(): Product
    {
        $collection = collect($this->products);
        $sorted = $collection->sortByDesc(function(Product $product, $key) {
            return $product->getRating();
        });

        return $sorted->values()->first();
    }

    public function getCheapestProducts(): array
    {
        $collection = collect($this->products);
        $sorted = $collection->sortBy(function(Product $product, $key) {
            return $product->getPrice();
        });

        return $sorted->values()->slice(0,3)->all();
    }
}
