<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    // TODO: Implement methods
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    
    public function execute(): GetCheapestProductsResponse
    {
        // TODO: Implement
        $cheapestProducts = $this->productRepository->getCheapestProducts();

        return new GetCheapestProductsResponse($cheapestProducts);
    }
}