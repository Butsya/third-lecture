<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetAllProductsAction
{
    // TODO: Implement methods
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetAllProductsResponse
    {
        // TODO: Implement
        $products = $this->productRepository->findAll();

        return new GetAllProductsResponse($products);
    }
}
