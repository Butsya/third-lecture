<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    // TODO: Implement methods
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        // TODO: Implement
        $product = $this->productRepository->findMostPopularProduct();

        return new GetMostPopularProductResponse($product);
    }
}
