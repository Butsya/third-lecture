<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        $products = [];

        for ($i = 0; $i < 10; $i++) {
            $products[] = new Product($i, 'product_' . $i, 2.00 * $i, 'image_url' . $i, 0.5 * $i);
        }

        return $products;
    }
}
