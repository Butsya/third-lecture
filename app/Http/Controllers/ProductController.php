<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    private $getAllProductsAction;
    private $getCheapestProductsAction;
    private $getMostPopularProductAction;

    public function __construct(
        GetAllProductsAction $getAllProductsAction,
        GetCheapestProductsAction $getCheapestProductsAction,
        GetMostPopularProductAction $getMostPopularProductAction
    )
    {
        $this->getAllProductsAction = $getAllProductsAction;
        $this->getCheapestProductsAction = $getCheapestProductsAction;
        $this->getMostPopularProductAction = $getMostPopularProductAction;
    }
    // TODO: Implement methods
    public function getAllProducts()
    {
        $response = $this->getAllProductsAction->execute();

        return response()->json(ProductArrayPresenter::presentCollection($response->getProducts()), 200);
    }

    public function getMostPopularProduct()
    {
        $response = $this->getMostPopularProductAction->execute();

        return response()->json(ProductArrayPresenter::present($response->getProduct()), 200);
    }

    public function getCheapestProducts()
    {
        $response = $this->getCheapestProductsAction->execute();

        return view('cheap_products', ['products' => $response->getProducts()]);
    }
}
