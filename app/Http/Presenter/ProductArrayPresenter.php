<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        // TODO: Implement
        return array_map(function (Product $product){
            return self::present($product);
        }, $products);
    }

    public static function present(Product $product): array
    {
        // TODO: Implement
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'img' => $product->getImageUrl(),
            'rating' => $product->getRating(),
        ];
    }
}
