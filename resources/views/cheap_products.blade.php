<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    List of cheap products
    <ul>
        @foreach($products as $product)
            <li>
                Name: {{ $product->getName() }}
                Price: {{ $product->getPrice() }}
                Rating: {{ $product->getRating() }}
            </li>
        @endforeach
    </ul>
</body>
</html>
